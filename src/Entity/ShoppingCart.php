<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingCartRepository")
 */
class ShoppingCart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $total;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LineProductCart", mappedBy="shoppingCarts", cascade={"persist"})
     */
    private $lineProductCarts;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="shoppingCarts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    public function __construct()
    {
        $this->lineProductCarts = new ArrayCollection();
        $this->isActive = true;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(?int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return Collection|LineProductCart[]
     */
    public function getLineProductCarts(): Collection
    {
        return $this->lineProductCarts;
    }

    public function addLineProductCart(LineProductCart $lineProductCart): self
    {
        if (!$this->lineProductCarts->contains($lineProductCart)) {
            $this->lineProductCarts[] = $lineProductCart;
            $lineProductCart->setShoppingCarts($this);
        }

        return $this;
    }

    public function removeLineProductCart(LineProductCart $lineProductCart): self
    {
        if ($this->lineProductCarts->contains($lineProductCart)) {
            $this->lineProductCarts->removeElement($lineProductCart);
            // set the owning side to null (unless already changed)
            if ($lineProductCart->getShoppingCarts() === $this) {
                $lineProductCart->setShoppingCarts(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
