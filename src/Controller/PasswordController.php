<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\PasswordProfileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordController extends Controller
{
    /**
     * @Route("/password/user", name="password")
     */
    public function index(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $form = $this->createForm(PasswordProfileType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            // $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute("home");
        }


        return $this->render('password/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
