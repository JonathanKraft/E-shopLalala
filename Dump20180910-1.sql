-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: db
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (2,123,'admin','admin',34555),(3,23,'user','user',2355),(4,1,'admin1','admin1',42355),(5,1,'admin2','admin2',1);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `address_user`
--

LOCK TABLES `address_user` WRITE;
/*!40000 ALTER TABLE `address_user` DISABLE KEYS */;
INSERT INTO `address_user` VALUES (2,1),(3,2),(4,3),(5,4);
/*!40000 ALTER TABLE `address_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `line_product_cart`
--

LOCK TABLES `line_product_cart` WRITE;
/*!40000 ALTER TABLE `line_product_cart` DISABLE KEYS */;
INSERT INTO `line_product_cart` VALUES (1,2,3,2,1),(2,2,4,1,1),(3,1,6,1,2);
/*!40000 ALTER TABLE `line_product_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20180830144813'),('20180830152322'),('20180830152603');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Turnip',2,'Turnip',NULL,'38a114ce806c75b73f88e709652c022b.png'),(2,'Turnip',1,'Turnip',NULL,'18b2af7a71e7a21ca77e0e9720c413a3.png');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `shopping_cart`
--

LOCK TABLES `shopping_cart` WRITE;
/*!40000 ALTER TABLE `shopping_cart` DISABLE KEYS */;
INSERT INTO `shopping_cart` VALUES (1,1,NULL,1),(2,2,NULL,1),(3,3,NULL,0),(4,3,NULL,0),(5,3,NULL,1),(6,4,NULL,1);
/*!40000 ALTER TABLE `shopping_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin','admin@admin.fr',1,'$2y$12$.nh26GqVe1mt8Nvrbxx9ZOSLbIPRIvOrxluSU3yZoS8RvfercglmK','ROLE_ADMIN'),(2,'user','user','user@user.fr',0,'$2y$12$eZz5ZSkjtugBJdIgNE0Sm.2slfkYGCLocUU6CgvqmaH2UcdDgWr4C','ROLE_USER'),(3,'admin1','admin1','admin1@admin1.fr',1,'$2y$12$nG6kCS7r.IMgivFTDxL.meEUDkFQZ7xqpvfjUCGncb7Bo8YUEnrnO','ROLE_ADMIN'),(4,'admin2','admin2','admin2@admin2.fr',1,'$2y$12$k4mb3bYeAcUMjI4S8TWXIezh1dB.XQ4QCSa9i9wcCcBUTybYoGYMe','a:1:{i:0;s:9:\"ROLE_USER\";}');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-10 16:49:12
